package com.brewengine.libgdx.playground.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.brewengine.libgdx.playground.PlaygroundGame;
import com.brewengine.libgdx.playground.PlaygroundGame.MainCallback;

/**
 * Android Activity life-cycle:
 * {@link http://developer.android.com/reference/android/app/Activity.html}
 */
public class PlaygroundAndroidGame extends AndroidApplication implements MainCallback {
	
	private static final String TAG = PlaygroundGame.TAG;
	private static final String CLASS = "PlaygroundAndroidGame";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		listener = new PlaygroundGame(this);
		
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useAccelerometer = false;
		config.useCompass = false;
		
		initialize(listener, config);
//		Gdx.input.setCatchBackKey(true);
		
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	};
	
	/**
	 * Occurs after an {@link #onStop}.
	 */
	@Override
	protected void onRestart() {
		super.onRestart();
		log(TAG, CLASS + ".onRestart");
	}
	
	public void onStart() {
		super.onStart();
		log(TAG, CLASS + ".onStart");
	}
	
	/**
	 * User returns to the activity.
	 * Occurs after a {@link #onPause}.
	 */
	@Override
	protected void onResume() {
		super.onResume();
		log(TAG, CLASS + ".onResume");
	}
	
	/*
	 * Activity is running after completion of {@link #onResume}.
	 */
	
	/**
	 * Another activity comes into the foreground.
	 */
	@Override
	protected void onPause() {
		super.onPause();
		log(TAG, CLASS + ".onPause");
	}
	
	/**
	 * The activity is no longer visible.
	 */
	public void onStop() {
		log(TAG, CLASS + ".onStop");
		super.onStop();
	}
	
	/**
	 * The activity is finished or being destroyed by the system.
	 */
	@Override
	protected void onDestroy() {
		log(TAG, CLASS + ".onDestroy");
		super.onDestroy();
	}
	
}
