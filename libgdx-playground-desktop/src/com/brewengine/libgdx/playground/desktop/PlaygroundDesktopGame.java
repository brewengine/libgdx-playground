package com.brewengine.libgdx.playground.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.brewengine.libgdx.playground.PlaygroundGame;
import com.brewengine.libgdx.playground.PlaygroundGame.MainCallback;

public class PlaygroundDesktopGame implements MainCallback {

	private static final String APPLICATION_TITLE = "libGDX Playground";

	public enum Device {
		Standard240p, // 16:9
		Standard360p, // 16:9
		Standard480p, // 16:9
		Standard720p, // 16:9
		Standard1080p, // 16:9
		YouTube,
		YouTubeSD,
		YouTubeHD,

		MotorolaDroid,
		MotorolaDroidX,
		MotorolaTriumph,
		NexusOne,
		LGOptimusV,
		SamsungGalaxyTab7, // 7 inch screen
		SamsungGalaxyTab10, // 10.1 inch screen
	}
	
	public static void main(String[] args) {
		ApplicationListener listener = new PlaygroundGame(new PlaygroundDesktopGame());
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = APPLICATION_TITLE;
		config.useGL20 = false;
		
		setResolution(config, Device.MotorolaDroid);
		
//		DisplayMode displayMode = LwjglApplicationConfiguration.getDesktopDisplayMode();
//		config.setFromDisplayMode(displayMode);
		
		new LwjglApplication(listener, config);
	}
	
	public static void setResolution(LwjglApplicationConfiguration config, Device device) {
		switch (device) {
		case Standard240p:
			config.width = 426;
			config.height = 240;
			break;
		case Standard360p:
			config.width = 640;
			config.height = 360;
			break;
		case Standard480p:
			config.width = 854;
			config.height = 480;
			break;
		case Standard1080p:
			config.width = 1980;
			config.height = 1080;
			break;
		case YouTube:
			config.width = 320;
			config.height = 240;
			break;
		case YouTubeSD:
			config.width = 640;
			config.height = 480;
			break;
		case YouTubeHD:
		case Standard720p:
			config.width = 1280;
			config.height = 720;
			break;
		case MotorolaDroid:
		case MotorolaDroidX:
			config.width = 480;
			config.height = 854;
			break;
		case MotorolaTriumph:
		case NexusOne:
			config.width = 480;
			config.height = 800;
			break;
		case LGOptimusV:
			config.width = 320;
			config.height = 480;
			break;
		case SamsungGalaxyTab7:
			config.width = 600;
			config.height = 1024;
			break;
		case SamsungGalaxyTab10:
			config.width = 800;
			config.height = 1280;
			break;
		}
	}
	
}
