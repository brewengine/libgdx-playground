package com.brewengine.libgdx.playground.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FakeLoadingScreen extends AbstractLoadingScreen {

	private final float durationSeconds;
	private float elapsedSeconds;
	private boolean isPaused;

	public FakeLoadingScreen(Game game, Screen nextScreen, float durationSeconds) {
		this(game, nextScreen, durationSeconds, null, null);
	}

	public FakeLoadingScreen(Game game, Screen nextScreen, float durationSeconds, LoadingScreenListener listener) {
		this(game, nextScreen, durationSeconds, null, listener);
	}
	
	public FakeLoadingScreen(Game game, Screen nextScreen, float durationSeconds, SpriteBatch batch) {
		this(game, nextScreen, durationSeconds, batch, null);
	}
	
	public FakeLoadingScreen(Game game, Screen nextScreen, float durationSeconds, SpriteBatch batch, LoadingScreenListener listener) {
		super(game, nextScreen, batch, listener);
		this.durationSeconds = durationSeconds;
	}

	public void reset() {
		elapsedSeconds = 0f;
	}
	
	@Override
	public boolean isDone() {
		return elapsedSeconds > durationSeconds;
	}

	@Override
	protected void updateProgress(float delta) {
		if (!isPaused) {
			elapsedSeconds += delta;
			progress = Math.min((int) ((elapsedSeconds / durationSeconds) * 100f), 100);
		}
	}

	@Override
	public void pause() {
		super.pause();
		isPaused = true;
	}

	@Override
	public void resume() {
		super.resume();
		isPaused = false;
	}

}
