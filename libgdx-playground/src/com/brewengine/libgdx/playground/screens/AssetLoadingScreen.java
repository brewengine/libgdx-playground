package com.brewengine.libgdx.playground.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;

public class AssetLoadingScreen extends AbstractLoadingScreen {

	protected final AssetManager assetManager;
	
	public AssetLoadingScreen(Game game, Screen nextScreen, AssetManager assetManager) {
		this(game, nextScreen, assetManager, null);
	}
	
	public AssetLoadingScreen(Game game, Screen nextScreen, AssetManager assetManager, LoadingScreenListener listener) {
		super(game, nextScreen, listener);
		
		if (assetManager == null) {
			throw new RuntimeException("Asset manager cannot be null.");
		}
		this.assetManager = assetManager;
	}
	
	@Override
	protected void updateProgress(float delta) {
		progress = (int) (assetManager.getProgress() * 100f);
	}
	
	@Override
	public boolean isDone() {
		return assetManager.update();
	}

}
