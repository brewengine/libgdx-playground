package com.brewengine.libgdx.playground.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;

public class DefaultScreen extends AbstractLoggingScreen implements Screen {

	public Color backgroundColor = new Color(Color.BLACK);
	
	@Override
	public void render(float delta) {
		GLCommon gl = Gdx.graphics.getGLCommon();
		clearScreen(gl);
	}

	protected void clearScreen(GLCommon gl) {
		gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	}

}
