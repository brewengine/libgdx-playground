package com.brewengine.libgdx.playground.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.brewengine.libgdx.playground.PlaygroundGame;

abstract public class AbstractLoadingScreen extends AbstractLoggingScreen implements Screen {
	
	private static final String TAG = PlaygroundGame.TAG;
	private static final String CLASS = "AbstractLoadingScreen";
	
	public Color backgroundColor = new Color(Color.BLACK);

	protected LoadingScreenListener loadingScreenListener;
	protected final Screen nextScreen;
	protected final Game game;
	
	private int width;
	private int height;
	
	protected BitmapFont loadingFont;
	protected final BitmapFontCache loadingFontCache;
	private final SpriteBatch batch;
	private boolean ownsBatch;
	
	protected int progress;
	private int lastProgress;
	
	public AbstractLoadingScreen(Game game, Screen nextScreen) {
		this(game, nextScreen, null, null);
	}
	
	public AbstractLoadingScreen(Game game, Screen nextScreen, LoadingScreenListener listener) {
		this(game, nextScreen, null, listener);
	}
	
	public AbstractLoadingScreen(Game game, Screen nextScreen, SpriteBatch batch) {
		this(game, nextScreen, batch, null);
		
	}
	
	public AbstractLoadingScreen(Game game, Screen nextScreen, SpriteBatch batch, LoadingScreenListener listener) {
		this.game = game;
		this.nextScreen = nextScreen;
		
		if (batch == null) {
			ownsBatch = true;
			batch = new SpriteBatch();
		}
		this.batch = batch;
		
		setupLoadingFont();
		if (loadingFont != null) {
			loadingFontCache = new BitmapFontCache(loadingFont);
		} else {
			loadingFontCache = null;
		}
		
		setListener(listener);
		
		this.width = Gdx.graphics.getWidth();
		this.height = Gdx.graphics.getHeight();
	}
	
	public void setListener(LoadingScreenListener listener) {
		this.loadingScreenListener = listener;
	}
	
	public int getProgress() {
		return progress;
	}
	
	protected void setupLoadingFont() {
		loadingFont = new BitmapFont();
	}
	
	@Override
	public void render(float delta) {
		GLCommon gl = Gdx.graphics.getGLCommon();
		clearScreen(gl);
		
		batch.begin();
		render(delta, batch);
		batch.end();
		
		tick(delta);
	}
	
	protected void clearScreen(GLCommon gl) {
		gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, backgroundColor.a);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
	}
	
	protected void render(float delta, SpriteBatch batch) {
		if (loadingFontCache != null) {
			if (progress != lastProgress) {
				lastProgress = progress;
				
				String text = "Loading ... " + progress + "%";
				TextBounds bounds = loadingFont.getBounds(text);
				float x = width / 2 - bounds.width / 2;
				float y = height/ 2 - bounds.height / 2;
				loadingFontCache.setText(text, x, y);
			}
			
			loadingFontCache.draw(batch);
		}
	}
	
	protected void tick(float delta) {
		if (isDone()) {
			debug(TAG, CLASS + ".tick: Done.");
			
			if (loadingScreenListener != null) {
				loadingScreenListener.doneLoading(this);
			}
			
			setScreen(nextScreen);
			
			Gdx.app.postRunnable(new Runnable() {
				@Override
				public void run() {
					dispose();
				}
			});
		} else {
			updateProgress(delta);
		}
	}
	
	protected void setScreen(Screen screen) {
		game.setScreen(screen);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		this.width = width;
		this.height = height;
		batch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		if (loadingFont != null) {
			loadingFont.dispose();
			loadingFont = null;
		}
		if (ownsBatch) {
			batch.dispose();
		}
	}
	
	abstract public boolean isDone();
	abstract protected void updateProgress(float delta);
	
	public interface LoadingScreenListener {
		void doneLoading(AbstractLoadingScreen screen);
	}

}
