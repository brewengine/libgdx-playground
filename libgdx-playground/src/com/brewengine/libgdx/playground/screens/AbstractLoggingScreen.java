package com.brewengine.libgdx.playground.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.brewengine.libgdx.playground.PlaygroundGame;

public abstract class AbstractLoggingScreen implements Screen {

	private static final String TAG = PlaygroundGame.TAG;
	private static final String CLASS = "AbstractLoggingScreen";
	
	@Override
	public void resize(int width, int height) {
		debug(TAG, CLASS + ".resize: this = " + this.getClass().getSimpleName() + ", width = " + width + ", height = " + height);
	}

	@Override
	public void show() {
		debug(TAG, CLASS + ".show: this = " + this.getClass().getSimpleName());
	}

	@Override
	public void hide() {
		debug(TAG, CLASS + ".hide: this = " + this.getClass().getSimpleName());
	}

	@Override
	public void pause() {
		debug(TAG, CLASS + ".pause: this = " + this.getClass().getSimpleName());
	}

	@Override
	public void resume() {
		debug(TAG, CLASS + ".resume: this = " + this.getClass().getSimpleName());
	}

	@Override
	public void dispose() {
		debug(TAG, CLASS + ".dispose: this = " + this.getClass().getSimpleName());
	}
	
	public void log(String tag, String message) {
		Gdx.app.log(tag, message);
	}
	
	public void error(String tag, String message) {
		Gdx.app.error(tag, message);
	}
	
	public void debug(String tag, String message) {
		Gdx.app.debug(tag, message);
	}

}
