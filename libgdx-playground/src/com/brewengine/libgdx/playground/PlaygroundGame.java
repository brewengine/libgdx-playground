package com.brewengine.libgdx.playground;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.brewengine.libgdx.playground.screens.DefaultScreen;
import com.brewengine.libgdx.playground.screens.FakeLoadingScreen;

public class PlaygroundGame extends Game {

	/**
	 * Per "good convention" recommendations found at:
	 * {@link http://developer.android.com/reference/android/util/Log.html}
	 */
	public static final String TAG = "libGDX Playground";
	
	private static final String CLASS = "PlaygroundGame";
	
	protected final MainCallback callback;
	
	public PlaygroundGame(MainCallback callback) {
		this.callback = callback;
	}
	
	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);
		Gdx.app.debug(TAG, CLASS + ".create");
		
//		setScreen(new DefaultScreen());
		setScreen(new FakeLoadingScreen(this, new DefaultScreen(), 5 /* seconds */));
	}
	
	@Override
	public void pause() {
		super.pause();
		Gdx.app.debug(TAG, CLASS + ".pause");
	}
	
	@Override
	public void resume() {
		super.resume();
		Gdx.app.debug(TAG, CLASS + ".resume");
	}
	
	@Override
	public void dispose() {
		Gdx.app.debug(TAG, CLASS + ".dispose");
		super.dispose();
	}
	
	public static interface MainCallback {
		
	}

}
